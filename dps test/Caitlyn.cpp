#include "stdafx.h"
#include "Caitlyn.h"
#include "Random"

#include <iostream>
using namespace std;

Caitlyn::Caitlyn()
{
}

int Caitlyn::crit_maker()
{
	int xRan;

	xRan = rand() % 100 + 1; // Randomizing the number between 1-100

	return xRan;
}

double Caitlyn::headshot_percentage(Caitlyn attacker)
{
	

	double crit_holder = attacker.crit_chance;   //make that into decimal form
	
	crit_holder /= 100;
	/* FOR WHEN IE BOOLEAN IS IMPLEMENTED
		if(attacker.isCrit == true)
		{
			headshot_percent = (.50 + crit_chance + .25);

			return headshot_percent;
		}
		else
		{
			headshot_percent = (.50 + crit_chance);
			return headshot_percent;
		}
	
	
	*/

	

	headshot_mod = (.50 + crit_holder);

	

	return headshot_mod;
	
}

double Caitlyn::attack(Caitlyn &attacker, Darius defender)
{
	int xRan = crit_maker();

	double headshot_output;

	double headshot_modifier = headshot_percentage(attacker);

	double damage = attacker.ad;  //setting base attack damage
								  //damage=item.getItems(damage);
	double damage_dealt = 0;

	


	//cout << attacker.crit_chance << endl;
	if (xRan < attacker.crit_chance) //if its a crit
	{
		attacker.headshot_marker++;
		
		cout << attacker.headshot_marker << endl;

		
		if (attacker.headshot_marker == 6)
		{
			attacker.headshot_marker = 0;

			damage_dealt = (damage * headshot_percentage(attacker) + damage * 2) * defender.armor_Reduction();
			cout << "you headshotted and crit" << endl;
			cout << "DMG: " << damage_dealt << "\n\n\n";
		}
		else
		{
			damage_dealt = (damage * 2) * defender.armor_Reduction();
			cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";
		}
			

		return damage_dealt;
	}
	else  //its not a crit
	{
		attacker.headshot_marker++;

		cout << attacker.headshot_marker << endl;

		
		if (attacker.headshot_marker == 6)
		{
			attacker.headshot_marker = 0;
			damage_dealt = (damage * headshot_percentage(attacker) + damage) * defender.armor_Reduction();

			headshot_output = damage * headshot_percentage(attacker);  //simply for output


			
			cout << "headshot damage" << "( " << headshot_output << " )" << endl;
			cout << "DMG: " << damage_dealt << "\n\n\n";
		}
		else
		{
			damage_dealt = damage * defender.armor_Reduction();
			cout << "DMG: " << damage_dealt << "\n\n\n";   //output for damage dealt
		}
			

		return damage_dealt;
	}
}

double Caitlyn::attack2(Caitlyn &attacker, Rek defender)
{
	int xRan = crit_maker();

	double headshot_output;

	double headshot_modifier = headshot_percentage(attacker);

	double damage = attacker.ad;  //setting base attack damage
								  //damage=item.getItems(damage);
	double damage_dealt = 0;




	//cout << attacker.crit_chance << endl;
	if (xRan < attacker.crit_chance) //if its a crit
	{
		attacker.headshot_marker++;

		cout << attacker.headshot_marker << endl;


		if (attacker.headshot_marker == 6)
		{
			attacker.headshot_marker = 0;

			damage_dealt = (damage * headshot_percentage(attacker) + damage * 2) * defender.armor_Reduction();
			cout << "you headshotted and crit" << endl;
			cout << "DMG: " << damage_dealt << "\n\n\n";
		}
		else
		{
			damage_dealt = (damage * 2) * defender.armor_Reduction();
			cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";
		}


		return damage_dealt;
	}
	else  //its not a crit
	{
		attacker.headshot_marker++;

		cout << attacker.headshot_marker << endl;


		if (attacker.headshot_marker == 6)
		{
			attacker.headshot_marker = 0;
			damage_dealt = (damage * headshot_percentage(attacker) + damage) * defender.armor_Reduction();

			headshot_output = damage * headshot_percentage(attacker);  //simply for output



			cout << "headshot damage" << "( " << headshot_output << " )" << endl;
			cout << "DMG: " << damage_dealt << "\n\n\n";
		}
		else
		{
			damage_dealt = damage * defender.armor_Reduction();
			cout << "DMG: " << damage_dealt << "\n\n\n";   //output for damage dealt
		}


		return damage_dealt;
	}
}
double Caitlyn::attack3(Caitlyn &attacker, Cho defender)
{
	int xRan = crit_maker();

	double headshot_output;

	double headshot_modifier = headshot_percentage(attacker);

	double damage = attacker.ad;  //setting base attack damage
								  //damage=item.getItems(damage);
	double damage_dealt = 0;




	//cout << attacker.crit_chance << endl;
	if (xRan < attacker.crit_chance) //if its a crit
	{
		attacker.headshot_marker++;

		cout << attacker.headshot_marker << endl;


		if (attacker.headshot_marker == 6)
		{
			attacker.headshot_marker = 0;

			damage_dealt = (damage * headshot_percentage(attacker) + damage * 2) * defender.armor_Reduction();
			cout << "you headshotted and crit" << endl;
			cout << "DMG: " << damage_dealt << "\n\n\n";
		}
		else
		{
			damage_dealt = (damage * 2) * defender.armor_Reduction();
			cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";
		}


		return damage_dealt;
	}
	else  //its not a crit
	{
		attacker.headshot_marker++;

		cout << attacker.headshot_marker << endl;


		if (attacker.headshot_marker == 6)
		{
			attacker.headshot_marker = 0;
			damage_dealt = (damage * headshot_percentage(attacker) + damage) * defender.armor_Reduction();

			headshot_output = damage * headshot_percentage(attacker);  //simply for output



			cout << "headshot damage" << "( " << headshot_output << " )" << endl;
			cout << "DMG: " << damage_dealt << "\n\n\n";
		}
		else
		{
			damage_dealt = damage * defender.armor_Reduction();
			cout << "DMG: " << damage_dealt << "\n\n\n";   //output for damage dealt
		}


		return damage_dealt;
	}
}

void Caitlyn::calculate3(Caitlyn attacker, Cho defender)
{
	int attack_count = 0;

	//if defender is living keep attacking
	while (defender.hp > 0)
	{

		cout << "HP: " << defender.hp << endl;

		//up the count, as we can hit living champion
		attack_count++;

		//the new hp is done after the dmg done
		defender.hp -= attacker.attack3(attacker, defender);


	} //end of while loop

	  //final output as the loop condition shows all hits but last in output 
	cout << "HP: " << defender.hp << "    DEAD!" << endl;

	cout << "it took you " << attack_count << " attacks , GG.";
}

void Caitlyn::calculate2(Caitlyn attacker, Rek defender)
{
	int attack_count = 0;

	//if defender is living keep attacking
	while (defender.hp > 0)
	{

		cout << "HP: " << defender.hp << endl;

		//up the count, as we can hit living champion
		attack_count++;

		//the new hp is done after the dmg done
		defender.hp -= attacker.attack2(attacker, defender);


	} //end of while loop

	  //final output as the loop condition shows all hits but last in output 
	cout << "HP: " << defender.hp << "    DEAD!" << endl;

	cout << "it took you " << attack_count << " attacks , GG.";
}

void Caitlyn::calculate(Caitlyn attacker, Darius defender)
{
	int attack_count = 0;

	//if defender is living keep attacking
	while (defender.hp > 0)
	{

		cout << "HP: " << defender.hp << endl;

		//up the count, as we can hit living champion
		attack_count++;

		//the new hp is done after the dmg done
		defender.hp -= attacker.attack(attacker, defender);


	} //end of while loop

	  //final output as the loop condition shows all hits but last in output 
	cout << "HP: " << defender.hp << "    DEAD!" << endl;

	cout << "it took you " << attack_count << " attacks , GG.";
}


double Caitlyn::get_inital_as()
{
	return initial_as;
}

double Caitlyn::get_initial_ad()
{
	return initial_ad;
}


Caitlyn::~Caitlyn()
{
}
