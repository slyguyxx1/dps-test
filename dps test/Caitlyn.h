#pragma once
#include "Rek.h"
#include "Darius.h"
#include "cho.h"
class Caitlyn
{
public:

	double initial_ad = 99.1;
	double initial_as = 1.01104;
	double modifier = 0;
	int crit_chance = 0;
	int headshot_marker = 0;
	int crit_maker();
	
	double &ad = initial_ad;
	double &as = initial_as;
	double &headshot_mod = modifier;
	int crit_ref = crit_chance;


	double attack(Caitlyn &attacker, Darius defender);
	double attack3(Caitlyn &attacker, Cho defender);
	double attack2(Caitlyn &attacker, Rek defender);
	double headshot_percentage(Caitlyn attacker);

	void calculate(Caitlyn attacker, Darius defender);
	void calculate3(Caitlyn attacker, Cho defender);
	void calculate2(Caitlyn attacker, Rek defender);
	double get_initial_ad();
	double get_inital_as();

	Caitlyn();
	~Caitlyn();
};

