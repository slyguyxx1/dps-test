#include "stdafx.h"
#include "Darius.h"


Darius::Darius()
{
}

double Darius::get_effective()
{
	double eHealth = (1 + (armor / 100)) * hp;

	return eHealth;
}

double Darius::armor_Reduction()
{
	if (armor >= 0)
	{
		return 100 / (100 + armor);
	}
	else
		return 2 - (100 / (100 - armor));
}
double Darius::magic_Reduction()
{
	if (magic >= 0)
	{
		return 100 / (100 + magic);
	}
	else
		return 2 - (100 / (100 - magic));
}


Darius::~Darius()
{
}
