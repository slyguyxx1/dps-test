#include "stdafx.h"
#include "Setup.h"

#include <iostream>
#include <cstdlib>
#include <string>


using namespace std;


Setup::Setup()
{
	

}

string Setup::get_attacker_list()
{
	return attacker_list[3];
}

string Setup::get_defender_list()
{
	return defender_list[3];
}

void Setup::set_attacker(string attackee)
{
	attacker = attackee;
}

void Setup::set_defender(string defendee)
{
	defender = defendee;
}

int Setup::attacker_prompt()
{
	int choice;
	cout << "Which attacker would you like:" << endl;
	cout << "1.) Vayne" << endl;
	cout << "2.) Caitlyn" << endl;
	cout << "3.) KogMaw" << endl;
	cin >> choice;

	return choice;

	
}

int Setup::defender_prompt()
{
	int choice;
	cout << "Which attacker would you like:" << endl;
	cout << "1.) Darius" << endl;
	cout << "2.) Nautilus" << endl;
	cout << "3.) Reksai" << endl;
	cin >> choice;

	return choice;
}

Setup::~Setup()
{
}
