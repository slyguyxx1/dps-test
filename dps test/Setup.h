#pragma once

#include <string>

using namespace std;

class Setup
{
public:

	string attacker_list[3]
	{
		"Vayne" "Caitlyn" "KogMaw"
	};

	string defender_list[3]
	{
		"Darius" "Nautilus" "Reksai"
	};

	Setup();

	string attacker;
	string defender;

	int attacker_prompt();
	int defender_prompt();

	string get_attacker_list();
	string get_defender_list();

	void set_attacker(string attackee);
	void set_defender(string defendee);

	~Setup();

};

