#pragma once

#include <math.h>  //for rounding of stats
#include "Darius.h"
#include "items.h"
#include "Rek.h"
/*
class stats
{
	double attackdmg;
	double attackspd;
	int crit;
	int life_steal;
	double armorPen;
	stats();

	~stats();







};
*/
class Vayne
{
public:
	//settings vars:
	
	double initial_ad = 92.2;
	double initial_as = .625 / .96;	
	int crit_chance_marker = 60;
	int crit_maker();
	int silver_bolts_marker = 0;

	double &ad = initial_ad; //set as a references for data tracking during function calls
	double &as = initial_as;

	int crit_chance = 0;
		


	Vayne(); //constructor
	//void Marker(Vayne &attacker);
	//int getMarker(Vayne attacker);
	double attack(Vayne &attacker,Darius defender); //specific champion attack function

	void calculate(Vayne attacker, Darius defender);

	double attack2(Vayne &attacker, Rek defender); //specific champion attack function
	double attack3(Vayne &attacker, Cho defender);
	void calculate2(Vayne attacker, Rek defender);
	void calculate3(Vayne attacker, Cho defender);

	double get_initial_ad();

	double get_initial_as();
	

	
	
	~Vayne();
};


