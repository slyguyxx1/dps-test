#include "stdafx.h"
#include "cho.h"

Cho::Cho()
{
}

double Cho::get_effective()
{
	double eHealth = (1 + (armor / 100)) * hp;

	return eHealth;
}

double Cho::armor_Reduction()
{
	if (armor >= 0)
	{
		return 100 / (100 + armor);
	}
	else
		return 2 - (100 / (100 - armor));
}
double Cho::magic_Reduction()
{
	if (magic >= 0)
	{
		return 100 / (100 + magic);
	}
	else
		return 2 - (100 / (100 - magic));
}


Cho::~Cho()
{
}
