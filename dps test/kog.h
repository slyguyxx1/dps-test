#pragma once
#include "Rek.h"
#include "Darius.h"
#include "cho.h"
class Kog
{
public:

	double initial_ad = 106;
	double initial_as = 1.30;//with q attack speed
	double initial_ap = 0;
	int crit_chance = 0;
	int crit_maker();
	double &ap = initial_ap;
	double &ad = initial_ad;
	double &as = initial_as;
	//double &headshot_mod = modifier;
	int crit_ref = crit_chance;


	double attack(Kog &attacker, Darius defender);
	double attack2(Kog &attacker, Rek defender);
	double attack3(Kog &attacker, Cho defender);
	//double headshot_percentage(Kog attacker);

	void calculate(Kog attacker, Darius defender);
	void calculate2(Kog attacker, Rek defender);
	void calculate3(Kog attacker, Cho defender);
	double get_initial_ad();
	double get_inital_as();

	Kog();
	~Kog();
};

